<?php

namespace Drupal\Tests\short_scale_formatter\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\short_scale_formatter\Plugin\Field\FieldFormatter\ShortScaleFormatter;

/**
 * Description of ShortScaleFormatterTest
 *
 * @author CosminFr
 * @group short_scale_formatter
 */
class ShortScaleFormatterTest extends UnitTestCase {

  /**
   * A ShortScaleFormatter object to use in this test class.
   *
   * @var \Drupal\short_scale_formatter\Plugin\Field\FieldFormatter\ShortScaleFormatter
   */
  protected $formatter;

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;
  
  protected function setUp() {
    parent::setUp();
//    $this->installEntitySchema('entity_test');

//   // Create and login a user that creates the content type.
//    $permissions = array(
//      'administer content types',
//      'administer node fields',
//      'administer node form display',
//      'administer node display',
//    );
//    parent::drupalLogin($this->drupalCreateUser($permissions));

    $this->entityManager = $this->getMockBuilder('\Drupal\Core\Entity\EntityManager')
        ->disableOriginalConstructor()
        ->getMock();
    $this->field = $this->getMockBuilder('\Drupal\field\Entity\FieldConfig')
        ->disableOriginalConstructor()
        ->getMock();
    
    $this->container = new \Drupal\Core\DependencyInjection\ContainerBuilder();
    $this->container->set('entity.manager', $this->entityManager);
    \Drupal::setContainer($this->container);
    
//    // Add a datetime range field.
//    $this->fieldStorage = FieldStorageConfig::create([
//      'field_name' => $this->randomMachineName(),
//      'entity_type' => 'entity_test',
//      'type' => 'integer',
//    ]);
//    $this->fieldStorage->save();
//
//    $this->field = FieldConfig::create([
//      'field_storage' => $this->fieldStorage,
//      'bundle' => 'entity_test',
//      'required' => TRUE,
//    ]);
//    $this->field->save();

    $plugin_id = 'short_scale_formatter';
    $plugin_definition = array();
    $field_definition = $this->field;
    $settings = array(
        'wrapper_element' => 'span',
        'wrapper_title_text' => '',
        'precision' => 1,
        'thousand' => 'k',
        'million'  => 'm',
        'billion'  => 'b',
        'trillion' => 't',
    );
        
    $this->formatter = new ShortScaleFormatter($plugin_id, $plugin_definition, $field_definition, $settings, '', '', array());
  }

  protected function tearDown() {
    $this->characters = NULL;
    $this->formatter = NULL;
  }

  protected function formatItemProvider() {
    return [ //Number, expected result, error message
      [-900,       '-900', '-900 should have no extra characters'],
      [100,         '100', '100 should have no extra characters'],
      [1000,         '1k', '1000 should have K suffix'],
      [1020,         '1k', '1000 should have K suffix'],
      [1240,       '1.2k', '1240 should have K suffix and 1 digit precision'],
      [1270,       '1.3k', '1270 should be rounded up'],
      [2345132,    '2.3m', '2345132 should have M suffix and 1 digit precision'],
      [454e8,     '45.4b', '454e8 "e" notation should also work'],
      ['-35499', '-35.5k', '"-35499" valid number from string data should be accepted'],
      ['234g',     '234g', 'non-numeric strings are returned "as is"'],
      [0,             '0', '0 should be OK'],
      [NULL,           '', 'NULL should return empty string'],
    ];
  }
  
  /*
   * @ data Provider formatItemProvider //dataProvider attribute not working as expected. 
   * Altough, the "foreach" work-around it not too bad either.
   */
  public function testFormatItem() {
    //$this->assertEquals($expected, $this->formatter->formatItem($number), $message);
    foreach ($this->formatItemProvider() as $data) {
      list($number, $expected, $message) = $data;
      $this->assertEquals($expected, $this->formatter->formatItem($number), $message);
    }
  }

}
