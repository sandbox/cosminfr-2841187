<?php

namespace Drupal\numeric_scale_formatter\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\numeric_scale_formatter\Entity\NumericScaleInterface;

/**
 * Defines the FormatType config entity.
 * 
 * @ConfigEntityType(
 *   id = "numeric_scale",
 *   label = @Translation("Numeric scale"),
 *   handlers = {
 *     "list_builder" = "Drupal\numeric_scale_formatter\Controller\NumericScaleListBuilder",
 *     "form" = {
 *        "add" = "Drupal\numeric_scale_formatter\Form\NumericScaleAddForm",
 *        "edit" = "Drupal\numeric_scale_formatter\Form\NumericScaleEditForm",
 *        "delete" = "Drupal\numeric_scale_formatter\Form\NumericScaleDeleteForm",
 *     }
 *   },
 *   config_prefix = "config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "factors" = "factors",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/user-interface/numeric-scale/{numeric_scale}",
 *     "delete-form" = "/admin/config/user-interface/numeric-scale/{numeric_scale}/delete",
 *   },
 * )
 *
 * @author CosminFr
 */
class NumericScale extends ConfigEntityBase implements NumericScaleInterface {
  
  /**
   * @var array
   */
  public $factors = array();
  
//  /**
//   * Overrides default "weight" sorting to use our scale factor.
//   */
//  public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
//    $factor_a = $a->get('factor') ?: 0;
//    $factor_b = $b->get('factor') ?: 0;
//    return ($factor_a > $factor_b) - ($factor_a < $factor_b);
//  }

  public function summary() {
    $result = '';
    foreach ($this->factors as $data) {
      if (empty($data['hidden']) && !empty($data['label'])) {
        $result .= $data['label'] . ', ';
      }
    }
    if (empty($result)) {
      return '';
    } else {
      return 'Scale factors: ' . $result;
    }
  }

  public function sample() {
    $factors = array_filter($this->factors, array($this, 'isUsable'));
    $randomfactor = array_rand($factors);
    $result = rand(1, 999) . $this->factors[$randomfactor]['label'];
    return $result;
  }
  
  protected function isHidden($factor) {
    if (is_array($factor)) {
      $hidden = !empty($factor['hidden']);
    } else {
      $hidden = FALSE;
    }
    return $hidden;
  }
  
  protected function isUsable($factor) {
    return !$this->isHidden($factor);
  }
  
  public function preSave(\Drupal\Core\Entity\EntityStorageInterface $storage) {
    parent::preSave($storage);
    self::sortByFactor($this->factors);
  }

  public static function sortByFactor(&$factors) {
    uasort($factors, function ($a, $b) {
      $va = empty($a['factor']) ? 0 : $a['factor'];
      $vb = empty($b['factor']) ? 0 : $b['factor'];;
      return ($va < $vb) - ($va > $vb);
    });
  }
}
