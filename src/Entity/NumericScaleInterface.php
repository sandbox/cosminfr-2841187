<?php

namespace Drupal\numeric_scale_formatter\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining NumericScale config entity
 * 
 * @author CosminFr
 */
interface NumericScaleInterface extends ConfigEntityInterface {
  //Add get/set methods for your configuration properties here.

  
}
