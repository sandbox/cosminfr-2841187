<?php


namespace Drupal\numeric_scale_formatter\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Description of NumericScaleListBuilder
 *
 * @author CosminFr
 */
class NumericScaleListBuilder extends ConfigEntityListBuilder {
  
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Numeric Scale');
    //$header['id'] = $this->t('Machine name');
    //TODO add more...
    $header['summary'] = $this->t('Summary');
    $header['sample'] = $this->t('Example');
    return $header + parent::buildHeader();
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    //$row['id'] = $entity->id();
    //TODO add more...
    $row['summary'] = $entity->summary();
    $row['sample']  = $entity->sample();
    return $row + parent::buildRow($entity);
  }
}
