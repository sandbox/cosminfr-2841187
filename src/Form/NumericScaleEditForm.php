<?php

namespace Drupal\numeric_scale_formatter\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\numeric_scale_formatter\Entity\NumericScale;

/**
 * Description of NumericScaleEditForm
 *
 * @author CosminFr
 */
class NumericScaleEditForm extends EntityForm {
  
  /**
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $elements = parent::form($form, $form_state);
    $numScale = $this->entity;

    $elements['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $numScale->label(),
      '#description' => t("Label for the numeric scale."),
      '#required' => TRUE,
    );
    $elements['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $numScale->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$numScale->isNew(),
    );

    $elements['factors'] = $this->buildFactors();
    // add Empty Row
    //$elements['factors']['_new_'] = $this->buildFactorRow();
    $elements['add'] = $this->buildEmptyRow();
    
    return $elements;
  }

  public function buildFactors() {
    $numScale = $this->entity;
    $factors  = $numScale->factors;
    \Drupal::logger('numeric_scale_formatter')->debug('buildFactors: ' . serialize($factors));
    $element = array(
      '#type' => 'table',
      '#title' => t('Scale'),
      '#header' => array(t('ID'), t('Factor'), t('Label'), t('Delete') ),
      '#empty' => t('This should not be empty. At minimum it should have a main "unit" factor (with value = 1).'),
    );
    foreach ($factors as $key => $data) {
      $data += array('factor' => NULL, 'label' => '');
      $element[$key]['ID'] = array(
        '#type' => 'textfield',
        '#title' => t('ID'),
        '#title_display' => 'invisible',
        '#size' => 20,
        '#default_value' => $key,
        '#disabled' => !empty($key),
      );
      $element[$key]['factor'] = array(
        '#type' => 'textfield',
        '#title' => t('Factor'),
        '#title_display' => 'invisible',
        '#default_value' => $data['factor'],
        '#attributes' => array('data-type' => 'number'),
//        '#required' => !empty($key),
      );
      $element[$key]['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Label'),
        '#title_display' => 'invisible',
        '#default_value' => $data['label'],
        '#required' => ($data['factor'] != 1),
      );
      $element[$key]['delete'] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete'),
        '#title_display' => 'invisible',
        '#disabled' => ($data['factor']==1), //should not remove main unit value
        '#default_value' => FALSE,
      );
    }
    return $element;
  }

  public function buildEmptyRow() {
    $element = array(
      '#type' => 'fieldset',
      '#title' => t('Add'),
      '#title_display' => 'invisible',
      '#attributes' => array('class' => array('container-inline')),
    );

    $element['new_ID'] = array(
      '#type' => 'textfield',
      '#title' => t('newID'),
      '#title_display' => 'invisible',
      '#placeholder' => t('Key'),
      '#size' => 20,
      '#default_value' => '',
    );
    $element['new_factor'] = array(
      '#type' => 'textfield',
      '#title' => t('Factor'),
      '#title_display' => 'invisible',
      '#placeholder' => t('Factor'),
      '#attributes' => array('data-type' => 'number'),
      '#default_value' => 1,
    );
    $element['new_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#title_display' => 'invisible',
      '#default_value' => '',
    );
  
    $element['button'] = array(
      '#type' => 'submit',
      '#title' => t('Add factor'),
      '#value' => t('Add factor'),
      '#submit' => array('::addFactor'),
//      '#ajax' => array(
//        'callback' => array($this, 'addFactor'),
//        'wrapper' => '#edit-factors',
//      ),
//      '#limit_validation_errors' => array(),
    );
    return $element;
  }

  public function addFactor(array $form, FormStateInterface $form_state) {
//    $response = new AjaxResponse();
//    $response->addCommand(new HtmlCommand('edit-label--description', 'Hello'));
//    return $response;
    
    $id     = $form_state->getValue('new_ID');
    if (!empty($id)) {
      $factor = $form_state->getValue('new_factor') ?: 1;
      $label  = $form_state->getValue('new_label');
      \Drupal::logger('numeric_scale_formatter')->debug('adding new factor: '. $id . ' ('.$factor.' as '.$label.')');
      $this->entity->factors[$id] = array('factor' => $factor, 'label' => $label);
      $this->entity->save();
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //Parse list of factors and remove those marked for deletion
    $factors = $form_state->getValue('factors');
    \Drupal::logger('numeric_scale_formatter')->debug('submitForm: ' . serialize($factors));
    foreach ($factors as $key => $data) {
//      if (empty($key) && !empty($data['ID'])) {
//        $key = $data['ID'];
//        $factors[$key] = $data;
//        unset($factors['']);
//      }
      if (!empty($data['delete']) || empty($data['factor'])) {
        unset($factors[$key]);
      }
    }
    //also check if there is a valid new factor to add.
    $id     = $form_state->getValue('new_ID');
    $factor = $form_state->getValue('new_factor') ?: 1;
    $label  = $form_state->getValue('new_label');
    if (!empty($id) && !empty($factor) && !empty($label)) {
      $factors[$id] = array('factor' => $factor, 'label' => $label);
    }
    //store updated values
    $form_state->setValue('factors', $factors);
    parent::submitForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    if ($status) {
      drupal_set_message($this->t('Saved the %label scale.', array(
        '%label' => $this->entity->label(),
      )));
    } else {
      drupal_set_message($this->t('The %label scale was not saved.', array(
        '%label' => $this->entity->label(),
      )));
    }
    $form_state->setRedirect('entity.numeric_scale.list');
  }

  public function exist($id) {
    $entity = $this->entityQuery->get('numeric_scale')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }
  
  public function xxx_options() {
    return array(
      'none' => t('Hide', array(), array('context' => 'datetime')),
      'estimate_label' => t('Estimate label', array(), array('context' => 'datetime')),
      'estimate_range' => t('Estimate range', array(), array('context' => 'datetime')),
      'estimate_component' => t('Start (single or from dates) or End (to dates) of estimate range', array(), array('context' => 'datetime')),
      'date_only' => t('Date component if set', array(), array('context' => 'datetime')),
      'date_or' => t('Date component with fallback to estimate component', array(), array('context' => 'datetime')),
    );
  }

}
