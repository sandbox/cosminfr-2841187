<?php

namespace Drupal\numeric_scale_formatter\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A form confirmation for deleting a Numeric Scale
 *
 * @author CosminFr
 */
class NumericScaleDeleteForm extends EntityConfirmFormBase {
  
  //this goes into title (less visible)
  public function getQuestion() {
    return $this->t('Delete scale %name ?',
        array('%name' => $this->entity->label() ) );
  }
  
  //this goes into the form (more visible)
  public function getDescription() {
    return $this->t('Are you sure you want to delete %name scale?',
        array('%name' => $this->entity->label() ) );
  }

  public function getCancelUrl() {
    return new \Drupal\Core\Url('entity.numeric_scale.list');
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    drupal_set_message($this->t('Numeric scale %label has been deleted.',
        array('%label' => $this->entity->label())));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }
}
