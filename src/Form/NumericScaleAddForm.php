<?php

namespace Drupal\numeric_scale_formatter\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\numeric_scale_formatter\Entity\NumericScale;

/**
 * Description of NumericScaleEditForm
 *
 * @author CosminFr
 */
class NumericScaleAddForm extends EntityForm {
  
  /**
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $elements = parent::form($form, $form_state);
    $numScale = $this->entity;

    $elements['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $numScale->label(),
      '#description' => t("Label for the numeric scale."),
      '#required' => TRUE,
    );
    $elements['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $numScale->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$numScale->isNew(),
    );

    $elements['clone'] = array(
      '#type' => 'select',
      '#title' => 'Copy factors from',
      '#default_value' => '',
      '#options' => $this->buildScaleOptions(),
      '#disabled' => !$numScale->isNew(),
    );
    
    return $elements;
  }

  public function buildScaleOptions() {
    $scales = $this->loadScales();
    $options[''] = t('Do not copy factors');
    foreach ($scales as $key => $scale) {
      $options[$key] = $scale->label; 
    }
    return $options;
  }

  protected function loadScales() {
    static $numericScales = NULL;
    if (!isset($numericScales)) {
      $storage = \Drupal::entityTypeManager()->getStorage('numeric_scale');
      $qry = \Drupal::entityQuery('numeric_scale')
        ->execute();
      $numericScales = $storage->loadMultiple($qry);
    }
    return $numericScales;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    //Check if we should clone factors from an existing scale
    $cloneFrom = $form_state->getValue('clone');
    if (!empty($cloneFrom)) {
      $scales = $this->loadScales();
      if (isset($scales[$cloneFrom])) {
        $this->entity->factors = $scales[$cloneFrom]->factors;
        $this->entity->save();
      }
    }
    parent::submitForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    if ($status) {
      drupal_set_message($this->t('Saved the %label scale.', array(
        '%label' => $this->entity->label(),
      )));
    } else {
      drupal_set_message($this->t('The %label scale was not saved.', array(
        '%label' => $this->entity->label(),
      )));
    }
    $form_state->setRedirect('entity.numeric_scale.edit_form', array('numeric_scale' => $this->entity->id()));
  }

  public function exist($id) {
    $entity = $this->entityQuery->get('numeric_scale')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
