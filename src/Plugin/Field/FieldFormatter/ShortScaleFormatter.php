<?php 

/**
 * @file
 * Contains \Drupal\numeric_scale_formatter\Plugin\Field\FieldFormatter\PartialDateFormatter.
 */

namespace Drupal\numeric_scale_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\numeric_scale_formatter\Plugin\Field\FieldFormatter\NumericScaleFormatter;

/**
 *
 * @FieldFormatter(
 *   id = "short_scale_formatter",
 *   module = "numeric_scale_formatter",
 *   label = @Translation("Short Scale"),
 *   description = @Translation("Format a number by shortening to most significant scale factor."),
 *   field_types = {"integer", "decimal", "float"},
 *   quickedit = {
 *     "editor" = "disabled"
 *   },
 *   settings = {
 *     "scale" = "",
 *     "factors" = {},
 *     "precision" = "0",
 *   },
 * )
 */
class ShortScaleFormatter extends NumericScaleFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'precision' => 2,
    ) + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $elements = parent::settingsForm($form, $form_state);
    $elements['precision'] = array(
      '#type' => 'number',
      '#title' => t('Precision'),
      '#description' => t('Specify how many digits should be displayed after decimal point. '),
      '#default_value' => $settings['precision'],
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 5,
    );
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = array();
    if (!empty($settings['scale'])) {
      $summary[] = t('Scale: <strong>@element</strong>', array('@element' => $settings['scale']));
    }
    if (!empty($settings['precision'])) {
      $summary[] = t('Precision: <strong>@element</strong>', array('@element' => $settings['precision']));
    }
    return $summary;
  }
  
  /**
   * {@inheritdoc}
   */
  public function formatItem($value, array $factors, int $precision = 0) {
    if (!is_numeric($value)) {
      return $value;
    } elseif (empty($value)) {
      return '';
    } else {
      $result = '';
      foreach($factors as $factor) {
        $fv = $value / $factor['factor'];
        if ($fv > 1) {
          //Round the value to specified precision
          $result .= round($fv, $precision) . $factor['label'];
          //Since this is "Short Scale" we only need one (the most significant) factor
          break; // nothing further to show.
        }
      }
      return $result;
    }
  }

}
