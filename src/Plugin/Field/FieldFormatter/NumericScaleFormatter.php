<?php 

/**
 * @file
 * Contains \Drupal\numeric_scale_formatter\Plugin\Field\FieldFormatter\NumericScaleFormatter.
 */

namespace Drupal\numeric_scale_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\numeric_scale_formatter\Entity\NumericScale;

/**
 *
 * @FieldFormatter(
 *   id = "numeric_scale_formatter",
 *   module = "numeric_scale_formatter",
 *   label = @Translation("Full Scale"),
 *   description = @Translation("Represent the number by appropriate scale factors."),
 *   field_types = {"integer", "decimal", "float"},
 *   quickedit = {
 *     "editor" = "disabled"
 *   },
 *   settings = {
 *     "scale" = "",
 *     "factors" = {},
 *     "precision" = "0",
 *   },
 * )
 */
class NumericScaleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'scale' => 'metric',
      'factors' => array(),
      'precision' => 0,
    ) + parent::defaultSettings();
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $elements = array();
    $elements['scale'] = array(
      '#type' => 'select',
      '#title' => t('Numeric scale'),
      '#default_value' => $settings['scale'],
      '#options' => $this->buildScaleOptions(),
      '#required' => TRUE,
    );
 //   $elements['factors'] = $this->buildFactorTable($form, $form_state);
    $elements['precision'] = array(
      '#type' => 'number',
      '#title' => t('Maximum factors shown'),
      '#description' => t('Use 0 to show all (as needed). Or choose a maximum number of factors to display. '),
      '#default_value' => $settings['precision'],
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 99,
    );
    return $elements;
  }

  public function buildFactorTable(array $form, FormStateInterface $form_state) {
    $table = array(
      '#type' => 'table',
      '#title' => t('Select usable factors'),
      '#description' => t('You can use @count within the string which will be replaced with the original number.'),
      '#header' => array(t('Usable factors') ),
      '#empty' => t('This should not be empty. Try selecting a different scale (above) and save.'),
      '#default_value' => $this->getSetting('factors'),
      '#tableselect' => TRUE,
      '#tabledrag' => FALSE,
      '#prefix' => '<div name="numeric-factors">',
      '#sufix' => '</div>',
    );
    $element = $form_state->getTriggeringElement();
    if (!empty($element) && !empty($element['#parents'])) {
      $selected = $form_state->getValue($element['#parents']);
    } else {
      $selected = $this->getSetting('scale');
    }
    $scale = $this->getScale($selected);
    if (!empty($scale)) {
      \Drupal::logger('numeric_scale_formatter')->debug('buildFactorTable for '.serialize($scale->label));
      foreach($scale->factors as $key => $factor) {
        $table[$key] = array('factor' => $factor['factor'], 'suffix' => $factor['label']);
      }
    } else {
      \Drupal::logger('numeric_scale_formatter')->debug('buildFactorTable could not find a scale! ');
    }
    return $table;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = array();
    if (!empty($settings['scale'])) {
      $summary[] = t('Scale: <strong>@element</strong>', array('@element' => $settings['scale']));
    }
//    $suffix = '';
//    foreach ($settings['factors'] as $factor) {
//      if (!empty($factor['label'])) {
//        $suffix .= t('&quot;@char&quot;, ', array('@char' => $factor['label']));
//      }
//    }
//    if (!empty($suffix)) {
//      $summary[] = t('Allowed suffixes: ' . $suffix);
//    }

    if (!empty($settings['precision'])) {
      $summary[] = t('Maximum factors shown: <strong>@element</strong>', array('@element' => $settings['precision']));
    }
    
    return $summary;
  }
  
  /**
   * {@inheritdoc}
   */
  public function viewElements(\Drupal\Core\Field\FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    
    if (!empty($settings['scale'])) {
      $scale = $this->getScale($settings['scale']);
      $factors = $scale->factors;
//      \Drupal::logger('numeric_scale_formatter')->debug('viewElements with: '. serialize($scale->label) . '<br> factors:' . serialize($factors));
      NumericScale::sortByFactor($factors);
    }
    $precision = !empty($settings['precision']) ? $settings['precision'] : 0;
    
    $element = array();
    foreach ($items as $delta => $item) {
      //$item is a numeric field;
      $value = $item->getValue();
      if (is_array($value) && isset($value['value'])) {
        $value = $value['value'];
      }
//      \Drupal::logger('numeric_scale_formatter')->debug('formating value: '. serialize($value) . '<br>precision:' . serialize($precision));
      $element[$delta][] = array('#markup' => $this->formatItem($value, $factors, $precision));
    }
    return $element;
  }
  
  /**
   * Formats one value using the given array of factors and precision
   * 
   * @param numeric $value
   *   The value we need to format.
   * @param array $factors
   *   The array of factors to use. This must be pre-sorted from most significant.
   * @param int $precision
   *   For Full Scale format, this specifies number of factors to show.
   *   For Short Scale format, this specifies number of digits after decimal point.
   * 
   */
  public function formatItem($value, array $factors, int $precision = 0) {
    if (!is_numeric($value)) {
      return $value;
    } elseif (empty($value)) {
      return '';
    } else {
      $result = '';
      $count  = 0;
      foreach($factors as $key => $factor) {
        $fv = floor($value / $factor['factor']);
        if ($fv > 1) {
          $result .= $fv . $factor['label'] . ' ';
          //reduce the value for further consideration
          $value = $value - ($fv * $factor['factor']);
          if ($value == 0) {
            break; // nothing further to show.
          }
          //check if maximum factors reached
          $count++;
          if (!empty($precision) && $count >= $precision) {
            break; //already shown maximum number of facors!
          }
        }
      }
      return $result;
    }
  }

  protected function loadScales() {
    static $numericScales = NULL;
    if (!isset($numericScales)) {
      $storage = \Drupal::entityTypeManager()->getStorage('numeric_scale');
      $qry = \Drupal::entityQuery('numeric_scale')
        ->execute();
      $numericScales = $storage->loadMultiple($qry);
    }
    return $numericScales;
  }

  protected function buildScaleOptions() {
    $scales = $this->loadScales();
    $options = array();
    foreach ($scales as $key => $scale) {
      $options[$key] = $scale->label; 
    }
    return $options;
  }

  protected function getScale($data) {
    $scales = $this->loadScales();
    foreach ($scales as $key => $scale) {
      if ($key == $data || $scale->label == $data) {
        return $scale; 
      }
    }
    return FALSE;
  }

}
